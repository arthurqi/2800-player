import java.util.List;

public abstract class Player {

    private int HP;
    private int XP;
    private int Level;
    private int Damage;
    private int Armour;
    private int mana;
    private int Ability;

    final int basehp = 600;
    final int baseDamage = 70;
    final int baseArmor = 30;

    public Player() {
        this.HP = basehp;
        this.XP = 0;
        this.Level = 0;
        this.Damage = baseDamage;
        this.Armour = baseArmor;
        this.mana = 0;
        this.Ability = 0;
    }

    public int getXP() {
        return XP;
    }

    public int getHP() {
        return HP;
    }

    public int getLevel() {
        return Level;
    }

    public int getArmour() {
        return Armour;
    }
    public void setArmour(int armourHcnage) {
        this.Armour += armourHcnage;
    }

    public int getAbility() {
        return Ability;
    }

    public int getMana() {
        return mana;
    }

    public int getDamage() {
        return Damage;
    }

    public void setHP(int HP) {
        this.HP += HP;
    }

    public void setMana(int mana) {
        this.mana += mana;
    }

    public abstract String getName();
    public abstract int skill(List<Player> otherPlayer);

    public void setDamage(int damage) {
        Damage += damage;
    }

    public void setXP(int XP) {
        this.XP -= XP;
    }

    public void levelUp() {
        this.Level++;
        this.Armour = this.Level * 15 + baseArmor;
        this.Damage = this.Level * 5 + baseDamage;
        this.HP = this.Level * 70 + basehp;

    }


}
