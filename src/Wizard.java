public abstract class Wizard extends Player{
    private String type;

    public Wizard() {
        this.type = "wizard";
    }

    public String getType() {
        return type;
    }
}
