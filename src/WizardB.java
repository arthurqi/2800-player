import java.util.List;

public class WizardB extends Wizard{
    private String name;
    public WizardB(){
        this.name = "wizardA";
    }

    public String getName() {
        return name;
    }

    public int  skill(List<Player> teamPlayer) {

        if (this.getMana() >= 80) {
            for(Player player : teamPlayer ) {
                player.setMana(10);
            }
            this.setMana(-80);
        }
        return this.getDamage();
    }
}
