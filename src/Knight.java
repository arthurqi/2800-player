public abstract class Knight extends Player{

    private String type;

    public Knight() {
        this.type = "knight";
    }

    public String getType() {
        return type;
    }
}
