import java.util.List;

public class KnightB extends Knight{

    private String name;
    public KnightB(){
        this.name = "knightB";

    }

    @Override
    public String getName() {
        return name;
    }

    public int  skill(List<Player> teamPlayer) {

        if (this.getMana() >= 60) {
            double times = 1;
            if(this.getLevel() <= 3) {
                times = 1.5;
            } else if (this.getLevel() > 8) {
                times = 2.1;
            } else {
                times = 1.8;
            }
            this.setArmour((int)(this.getArmour() * times));
            this.setMana(-60);
        }
        return this.getDamage();
    }
}
