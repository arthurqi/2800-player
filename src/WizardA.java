import java.util.List;

public class WizardA extends Wizard{
    private String name;
    public WizardA(){
        this.name = "wizardA";
    }

    public String getName() {
        return name;
    }

    public int  skill(List<Player> teamPlayer) {
        int manaNeed = 0;
        if(this.getLevel() <= 3) {
            manaNeed = 70;
        } else if (this.getLevel() > 8) {
            manaNeed = 45;
        } else {
            manaNeed = 60;
        }
        if (this.getMana() >= manaNeed) {
            for(Player player : teamPlayer ) {
                player.setHP(100);
            }
            this.setMana(0 - manaNeed);
        }
        return this.getDamage();
    }
}
