import java.util.List;

public interface PlayerOperation {
    /**
     * the attacker do the damage on the other. the real HP will reduece will be
     * attacker's damage - insured's armour.
     * <p>
     * <p>
     * also The attacker will get 10 mana for one basic attack
     *
     * @param attacker
     * @param injured
     * @param type       can be "basic" or "skill", if the skill will not give a damage. the skill
     *                   will do a basic attack and give effect
     * @param teamPlayer all players in the team. when the the attack is skill and and there
     *                   are some effect to other players, this will be useful
     */
    static void doDamage(Player attacker, Player injured, String type, List<Player> teamPlayer) throws Exception {
        int trueDamage = 0;
        if (type.equals("basic")) {
            trueDamage = attacker.getDamage() - injured.getArmour();
        } else if (type.equals("skill")) {
            trueDamage = attacker.skill(teamPlayer) - injured.getArmour();
        } else {
            throw new Exception();
        }
        if (trueDamage > 0) {
            injured.setHP(0 - trueDamage);
        }
        attacker.setMana(10);
    }

    /**
     * TO show the player is dead or not, based on the player's HP
     *
     * @param player
     * @return
     */
    static boolean deadOrNot(Player player) {
        return player.getHP() > 0;
    }

}
