import java.util.List;

public class KnightA extends Knight{

    private String name;
    public KnightA(){
        this.name = "knightA";
    }

    @Override
    public String getName() {
        return name;
    }

    public int  skill(List<Player> teamPlayer) {
        if (this.getMana() >= 50) {
            double times = 1;
            if(this.getLevel() <= 3) {
                times = 2;
            } else if (this.getLevel() > 8) {
                times = 2.8;
            } else {
                times = 2.3;
            }
            System.out.println((this.getDamage() * times));
            this.setMana(0 - 50);
            return (int)(this.getDamage() * times);
        }
        return this.getDamage();
    }
}
