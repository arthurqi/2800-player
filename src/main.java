import java.util.ArrayList;

public class main implements PlayerOperation{
    public static void main(String[] args) throws Exception {
        WizardA a = new WizardA();
        KnightA c = new KnightA();
        KnightA d = new KnightA();
        ArrayList<Player> t1 = new ArrayList<Player>();
        t1.add(a);
        t1.add(c);
        t1.add(d);
        KnightA b = new KnightA();
        a.setMana(100);
        PlayerOperation.doDamage(a, b, "skill",t1);
        System.out.println(b.getHP());
        System.out.println(a.getHP());
    }
}
